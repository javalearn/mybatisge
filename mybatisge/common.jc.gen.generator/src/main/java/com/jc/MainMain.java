package com.jc;

import com.jc.gen.GenProjectCode;

public class MainMain {
	
	
	public static void main(String[] args) throws Exception {
//		new GenCode().gen();
		long begin = System.currentTimeMillis();
		new GenProjectCode().gen();
		long end = System.currentTimeMillis();
		System.out.println("cost:" + (end - begin));
	}
}
