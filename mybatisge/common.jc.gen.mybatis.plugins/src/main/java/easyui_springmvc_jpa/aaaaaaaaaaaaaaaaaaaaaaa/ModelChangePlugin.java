package easyui_springmvc_jpa.aaaaaaaaaaaaaaaaaaaaaaa;

import java.util.List;

import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.PluginAdapter;
import org.mybatis.generator.api.dom.java.Field;
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.api.dom.java.JavaVisibility;
import org.mybatis.generator.api.dom.java.Method;
import org.mybatis.generator.api.dom.java.TopLevelClass;

/**
 * @author Jiangchi
 * 
 */
public class ModelChangePlugin extends PluginAdapter {

	@Override
	public boolean modelBaseRecordClassGenerated(TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {

		Method method = new Method(); 
		method.setName("toString");
		method.setVisibility(JavaVisibility.PUBLIC);
		method.setReturnType(FullyQualifiedJavaType.getStringInstance());
		method.addAnnotation("@Override");
		
		method.addBodyLine("return \"" + topLevelClass.getType().getShortName() + " [\"");
		List<Field> fs = topLevelClass.getFields();
		for (Field f : fs) {
			method.addBodyLine("+ \", " + f.getName() + "=\" + " + f.getName() + " ");
		}
		method.addBodyLine("+ \"]\";");
		
		topLevelClass.addMethod(method);
		return super.modelBaseRecordClassGenerated(topLevelClass, introspectedTable);
	}

	public boolean validate(List<String> warnings) {
		return true;
	}
}
