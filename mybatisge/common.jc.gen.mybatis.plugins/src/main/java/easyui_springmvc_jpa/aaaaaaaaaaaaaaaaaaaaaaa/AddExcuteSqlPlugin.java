package easyui_springmvc_jpa.aaaaaaaaaaaaaaaaaaaaaaa;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.mybatis.generator.api.IntrospectedTable;
import org.mybatis.generator.api.PluginAdapter;
import org.mybatis.generator.api.dom.java.FullyQualifiedJavaType;
import org.mybatis.generator.api.dom.java.Interface;
import org.mybatis.generator.api.dom.java.JavaVisibility;
import org.mybatis.generator.api.dom.java.Method;
import org.mybatis.generator.api.dom.java.Parameter;
import org.mybatis.generator.api.dom.java.TopLevelClass;
import org.mybatis.generator.api.dom.xml.Document;
import org.mybatis.generator.api.dom.xml.Element;
import org.mybatis.generator.api.dom.xml.XmlElement;

/**
 * @author Jiangchi
 * 
 */
public class AddExcuteSqlPlugin extends PluginAdapter {

	@Override
	public boolean clientGenerated(Interface interfaze, TopLevelClass topLevelClass, IntrospectedTable introspectedTable) {

		Method executeUpdateBySqlMethod = new Method();
		executeUpdateBySqlMethod.setVisibility(JavaVisibility.PUBLIC);
		executeUpdateBySqlMethod.setReturnType(FullyQualifiedJavaType.getIntInstance());
		executeUpdateBySqlMethod.setName("executeUpdateBySql");
		FullyQualifiedJavaType executeUpdateBySqlMethodParamType = FullyQualifiedJavaType.getNewMapInstance();
		executeUpdateBySqlMethodParamType.addTypeArgument(FullyQualifiedJavaType.getStringInstance());
		executeUpdateBySqlMethodParamType.addTypeArgument(FullyQualifiedJavaType.getStringInstance());
		executeUpdateBySqlMethod.addParameter(new Parameter(executeUpdateBySqlMethodParamType, "map"));

		Method executeSqlMethod = new Method();
		executeSqlMethod.setVisibility(JavaVisibility.PUBLIC);
		FullyQualifiedJavaType executeSqlMethodResultType = FullyQualifiedJavaType.getNewListInstance();
		FullyQualifiedJavaType executeSqlMethodResultSubType = FullyQualifiedJavaType.getNewMapInstance();
		executeSqlMethodResultSubType.addTypeArgument(FullyQualifiedJavaType.getStringInstance());
		executeSqlMethodResultSubType.addTypeArgument(FullyQualifiedJavaType.getObjectInstance());
		executeSqlMethodResultType.addTypeArgument(executeSqlMethodResultSubType);
		executeSqlMethod.setReturnType(executeSqlMethodResultType);
		executeSqlMethod.setName("executeSql");
		FullyQualifiedJavaType executeSqlMethodParamType = FullyQualifiedJavaType.getNewMapInstance();
		executeSqlMethodParamType.addTypeArgument(FullyQualifiedJavaType.getStringInstance());
		executeSqlMethodParamType.addTypeArgument(FullyQualifiedJavaType.getStringInstance());
		executeSqlMethod.addParameter(new Parameter(executeSqlMethodParamType, "map"));

		Set<FullyQualifiedJavaType> importedTypes = new TreeSet<FullyQualifiedJavaType>();
		importedTypes.add(FullyQualifiedJavaType.getNewMapInstance());
		interfaze.addImportedTypes(importedTypes);
		interfaze.addMethod(executeUpdateBySqlMethod);
		interfaze.addMethod(executeSqlMethod);
		return true;
	}

	@Override
	public boolean sqlMapDocumentGenerated(Document document, IntrospectedTable introspectedTable) {
		XmlElement parentElement = document.getRootElement();
		Element executeSql = new Element() {
			@Override
			public String getFormattedContent(int indentLevel) {
				return "  <select id=\"executeSql\" parameterType=\"java.util.Map\" resultType=\"java.util.Map\">\r\n  	${sql}\r\n  </select>";
			}
		};
		parentElement.addElement(executeSql);
		Element executeUpdateBySql = new Element() {
			@Override
			public String getFormattedContent(int indentLevel) {
				return "  <update id=\"executeUpdateBySql\" parameterType=\"java.util.Map\">\r\n  	${sql}\r\n  </update>";
			}
		};
		parentElement.addElement(executeUpdateBySql);
		return super.sqlMapDocumentGenerated(document, introspectedTable);
	}

	public boolean validate(List<String> warnings) {
		return true;
	}
}
